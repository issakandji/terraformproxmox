# terraformProxmox

Deploy promox Debian/ Unbuntu VMs using Terraform



Following provider have been used in 2.0.9
https://github.com/Telmate/terraform-provider-proxmox

Steps:
- Setup proxmox user
- Setup API Token for the user
- Give permission do the Token<br>
PS: User permission must precede API Token permission.

https://pve.proxmox.com/pve-docs/pveum-plain.html


# Templating
## Custom

https://pve.proxmox.com/wiki/Cloud-Init_FAQ#Creating_a_custom_cloud_image


## Image Ready

https://github.com/Telmate/terraform-ubuntu-proxmox-iso


# Thanks
<p>@Telmate/ Thanks for this provider<br>
@yetiops.net/ Thanks for the Debian-Cloud-init contribution. Learning a lot from your posts<br>
@austinsnerdythings/ Thanks for your deep dives<p>


- [ ] Review ressourceTemplate 
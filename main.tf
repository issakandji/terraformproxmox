terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9.0"
    }
  }
}

provider "proxmox" {
  pm_log_enable = true
  pm_log_file = "terraform-plugin-proxmox.log"
  pm_debug = true
  pm_log_levels = {
    _default = "debug"
    _capturelog = "" }
  pm_parallel       = 1
  pm_api_url = var.pm_api_url

  pm_api_token_id = var.pm_api_user_id
  #pm_user = var.pm_user_test
  pm_api_token_secret = var.pm_api_token_secret
  #pm_password = var.pm_password_test
  pm_tls_insecure = true
}

################################ Ubuntu Server ###################

resource "proxmox_vm_qemu" "sig-server00" {
  count = 1
  name = "sig-vm-${count.index + 1}"
  target_node = var.proxmox_host
  clone = var.template_Ubuntu
  agent = 1
  os_type = "cloud-init"
  cores = 2
  sockets = 1
  cpu = "host"
  memory = 2048
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  ciuser = "hostsadmin"
  sshkeys = <<EOF
  ${var.ssh_key}
  EOF
  


  disk {
    slot = 0
    size = "50G"
    type = "scsi"
    storage = "m1ZfsPool"
    iothread = 1
  }
  
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }
  
  ipconfig0 = "ip=10.0.0.5${count.index + 1}/24,gw=10.0.0.1"


}

################################## Debian Server ################
################### sig-server ressourceConfigurationsDefinition
# Source the Cloud Init Config file
data "template_file" "cloud_init_debian10_sig-server01" {
  template  = "${file("${path.module}/cloud_init_debian10.cloud_config")}"

  vars = {
    ssh_key = file("~/.ssh/id_rsa.pub")
    hostname = "sig-vm-2"
    domain = "localdomain"
  }
}

# Create a local copy of the file, to transfer to Proxmox
resource "local_file" "cloud_init_debian10_sig-server01" {
  content   = data.template_file.cloud_init_debian10_sig-server01.rendered
  filename  = "${path.module}/user_data_cloud_init_debian10_sig-server01.cfg"
}

# Transfer the file to the Proxmox Host
resource "null_resource" "cloud_init_debian10_sig-server01" {
  connection {
    type    = "ssh"
    user    = var.proxmox_user
    private_key = file("~/.ssh/id_rsa")
    host    = var.proxmox_host
    port    = var.proxmox_port
  }

  provisioner "file" {
    source       = local_file.cloud_init_debian10_sig-server01.filename
    destination  = "/var/lib/vz/snippets/cloud_init_debian10_sig-server01.yml"
  }
}

########################## VMS Definitions
resource "proxmox_vm_qemu" "sig-server01" {

  depends_on = [
    null_resource.cloud_init_debian10_sig-server01
  ]
  cicustom     = "user=local:snippets/cloud_init_debian10_sig-server01.yml"  
  count = 1
  name = "sig-vm-${count.index + 2}"
  target_node = var.proxmox_host
  clone = var.template_Debian
  agent = 1
  os_type = "cloud-init"
  cores = 2
  sockets = 1
  cpu = "host"
  memory = 2048
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  ciuser = "hostsadmin"
  cipassword = var.hostsadmin_pass
  sshkeys = <<EOF
  ${var.ssh_key}
  EOF

  disk {
    slot = 0
    size = "50G"
    type = "scsi"
    storage = "m1ZfsPool"
    iothread = 1
  }
  
  network {
    model = "virtio"
    bridge = "vmbr0"
  }

  lifecycle {
    ignore_changes = [
      network,
    ]
  }

  


  ipconfig0 = "ip=10.0.0.5${count.index + 2}/24,gw=10.0.0.1"
  

}